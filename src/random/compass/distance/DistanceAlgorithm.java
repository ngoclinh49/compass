package random.compass.distance;

public class DistanceAlgorithm
{
    private static double PIx = 3.141592653589793;
    private static double RADIUS = 6378.16;

    /// <summary>
    /// This class cannot be instantiated.
    /// </summary>
    private DistanceAlgorithm() { }

    /// <summary>
    /// Convert degrees to Radians
    /// </summary>
    /// <param name="x">Degrees</param>
    /// <returns>The equivalent in radians</returns>
    public static double Radians(double x)
    {
	return x * PIx / 180;
    }

    /// <summary>
    /// Calculate the distance between two places.
    /// </summary>
    /// <param name="lon1"></param>
    /// <param name="lat1"></param>
    /// <param name="lon2"></param>
    /// <param name="lat2"></param>
    /// <returns></returns>
    public static double DistanceBetweenPlaces(
	    double lon1,
	    double lat1,
	    double lon2,
	    double lat2)
    {
	double dlon = Radians(lon2 - lon1);
	double dlat = Radians(lat2 - lat1);

	double a = (Math.sin(dlat / 2) * Math.sin(dlat / 2)) + Math.cos(Radians(lat1)) * Math.cos(Radians(lat2)) * (Math.sin(dlon / 2) * Math.sin(dlon / 2));
	double angle = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
	return angle * RADIUS;
    }
}