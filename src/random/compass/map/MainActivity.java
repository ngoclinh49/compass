package random.compass.map;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.GeomagneticField;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnSeekCompleteListener;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
public class MainActivity extends Activity implements SensorEventListener {

    // define the display assembly compass picture
    private ImageView image;

    // record the compass picture angle turned
    private float currentDegree = 0f;
    private GoogleMap googleMap;

    // device sensor manager
    private SensorManager mSensorManager;
    Location myLoc, mapLoc;
    MediaPlayer mMediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_main);

	mapLoc = new Location("Gazza");
	mapLoc.setLatitude(31.5225605);
	mapLoc.setLongitude(34.453593);

	myLoc = new Location("Viet Nam");
	myLoc.setLatitude(15.9030623);
	myLoc.setLongitude(105.8066925);

	// our compass image 
	image = (ImageView) findViewById(R.id.imageViewCompass);

	// TextView that will tell the user what degree is he heading

	// initialize your android device sensor capabilities
	mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
	initmaps();

	final Button btnStart = (Button)findViewById(R.id.btnStartCount);
	btnStart.setOnClickListener(new OnClickListener() {

	    @Override
	    public void onClick(View v) {
		// TODO Auto-generated method stub
		btnStart.setVisibility(View.GONE);
		((TextView)findViewById(R.id.timeCountDown)).setVisibility(View.VISIBLE);
		startTimer(90000);
		sosSound();
	    }
	});
    }

    private void initmaps(){
	try {
	    // Loading map
	    initilizeMap();

	    // Changing map type
	    googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
	    //	     googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
	    // googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
	    // googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
	    // googleMap.setMapType(GoogleMap.MAP_TYPE_NONE);

	    // Showing / hiding your current location
	    googleMap.setMyLocationEnabled(true);

	    // Enable / Disable zooming controls
	    googleMap.getUiSettings().setZoomControlsEnabled(false);

	    // Enable / Disable my location button
	    googleMap.getUiSettings().setMyLocationButtonEnabled(true);

	    // Enable / Disable Compass icon
	    googleMap.getUiSettings().setCompassEnabled(true);

	    // Enable / Disable Rotate gesture
	    googleMap.getUiSettings().setRotateGesturesEnabled(true);

	    // Enable / Disable zooming functionality
	    googleMap.getUiSettings().setZoomGesturesEnabled(true);

	    double latitude = 17.385044;
	    double longitude = 78.486671;

	    // lets place some 10 random markers
	    for (int i = 0; i < 10; i++) {
		// random latitude and logitude
		double[] randomLocation = createRandLocation(latitude,
			longitude);

		// Adding a marker
		MarkerOptions marker = new MarkerOptions().position(
			new LatLng(latitude, longitude)).title("You ");

		// changing marker color
		marker.icon(BitmapDescriptorFactory
			.defaultMarker(BitmapDescriptorFactory.HUE_RED));

		googleMap.addMarker(marker);

		// Move the camera to last position with a zoom level
		CameraPosition cameraPosition = new CameraPosition.Builder()
		.target(new LatLng(latitude, longitude)).zoom(15).build();

		googleMap.animateCamera(CameraUpdateFactory
			.newCameraPosition(cameraPosition));

	    }

	} catch (Exception e) {
	    e.printStackTrace();
	}
    }


    void getCurrentLocation() {
	Location myLocation  = googleMap.getMyLocation();
	if(myLocation!=null)
	{
	    double dLatitude = myLocation.getLatitude();
	    double dLongitude = myLocation.getLongitude();
	    Log.i("APPLICATION"," : "+dLatitude);
	    Log.i("APPLICATION"," : "+dLongitude);
	    googleMap.addMarker(new MarkerOptions().position(
		    new LatLng(dLatitude, dLongitude)).title("My").icon(BitmapDescriptorFactory
			    .defaultMarker(BitmapDescriptorFactory.HUE_RED)));
	    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(dLatitude, dLongitude), 15));

	}
	else
	{
	    Toast.makeText(this, "Unable to fetch the current location", Toast.LENGTH_SHORT);
	}

    }


    @Override
    protected void onPause() {
	super.onPause();

	// to stop the listener and save battery
	mSensorManager.unregisterListener(this);
    }
    @Override
    public void onSensorChanged(SensorEvent event) {

	// Long code here --------------------------------------------//
	if (myLoc == null)
	    return;

	float azimuth = event.values[0];
	//		float baseAzimuth = azimuth;

	GeomagneticField geoField = new GeomagneticField(Double.valueOf(
		myLoc.getLatitude()).floatValue(), Double.valueOf(
			myLoc.getLongitude()).floatValue(), Double.valueOf(
				myLoc.getAltitude()).floatValue(), System.currentTimeMillis());

	// converts magnetic north into true north
	azimuth -= geoField.getDeclination();

	// Store the bearingTo in the bearTo variable
	float bearTo = myLoc.bearingTo(mapLoc);

	// If the bearTo is smaller than 0, add 360 to get the rotation
	// clockwise.
	if (bearTo < 0) {
	    bearTo = bearTo + 360;
	}

	// This is where we choose to point it
	float direction = bearTo - azimuth;

	// If the direction is smaller than 0, add 360 to get the rotation
	// clockwise.
	if (direction < 0) {
	    direction = direction + 360;
	}

//	rotateImageView(image, R.drawable.compass_android, direction);

    }
    //This is the method to Show the Image as Compass
    private void rotateImageView( ImageView imageView, int drawable, float rotate ) {

	// Decode the drawable into a bitmap
	Bitmap bitmapOrg = BitmapFactory.decodeResource( getResources(),drawable );

	// Get the width/height of the drawable
	DisplayMetrics dm = new DisplayMetrics(); 
	getWindowManager().getDefaultDisplay().getMetrics(dm);
	int width = bitmapOrg.getWidth(), height = bitmapOrg.getHeight();

	// Initialize a new Matrix
	Matrix matrix = new Matrix();

	// Decide on how much to rotate
	rotate = rotate % 360;

	// Actually rotate the image
	matrix.postRotate( rotate, width, height );

	// recreate the new Bitmap via a couple conditions
	Bitmap rotatedBitmap = Bitmap.createBitmap( bitmapOrg, 0, 0, width, height, matrix, true );
	//BitmapDrawable bmd = new BitmapDrawable( rotatedBitmap );

	//imageView.setImageBitmap( rotatedBitmap );
	imageView.setImageDrawable(new BitmapDrawable(getResources(), rotatedBitmap));
	imageView.setScaleType( ScaleType.CENTER );
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
	// not in use
    }

    @Override
    protected void onResume() {
	super.onResume();
	initilizeMap();
	// for the system's orientation sensor registered listeners
	mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION),
		SensorManager.SENSOR_DELAY_GAME);
    }

    /**
     * function to load map If map is not created it will create it for you
     * */
    private void initilizeMap() {
	if (googleMap == null) {
	    googleMap = ((MapFragment) getFragmentManager().findFragmentById(
		    R.id.map)).getMap();

	    // check if map is created successfully or not
	    if (googleMap == null) {
		Toast.makeText(getApplicationContext(),
			"Sorry! unable to create maps", Toast.LENGTH_SHORT)
			.show();
	    }
	}
    }

    /*
     * creating random postion around a location for testing purpose only
     */
    private double[] createRandLocation(double latitude, double longitude) {

	return new double[] { latitude + ((Math.random() - 0.5) / 500),
		longitude + ((Math.random() - 0.5) / 500),
		150 + ((Math.random() - 0.5) * 10) };
    }

    /**
     * Play sound on close door
     */
    private void sosSound() {
	mMediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.sos);
	mMediaPlayer.setLooping(true);
	mMediaPlayer.start();
    }

    CountDownTimer countDownTimer;
    private void startTimer(long time) {
	countDownTimer = new CountDownTimer(time, 1000) {
	    @Override
	    public void onTick(long leftTimeInMilliseconds) {
		int seconds = (int)(leftTimeInMilliseconds / 1000);
		String string = String.format("%02d:%02d", seconds /60,seconds % 60);
		((TextView)findViewById(R.id.timeCountDown)).setText(string);
	    }

	    @Override
	    public void onFinish() {
		String string = String.format("%02d:%02d", 10,0);
		((TextView)findViewById(R.id.timeCountDown)).setText(string);

		((TextView)findViewById(R.id.behideWall)).setText("Behide wall");
		((TextView)findViewById(R.id.behideWall)).setTextColor(Color.WHITE);
		if(mMediaPlayer!=null)
		    mMediaPlayer.release();
		startTimer10Minus(600000);
	    }
	}.start();
    }

    CountDownTimer countDownTimer10minus;
    private void startTimer10Minus(long time) { 
	countDownTimer10minus = new CountDownTimer(time, 1000) {
	    @Override
	    public void onTick(long leftTimeInMilliseconds) {
		int seconds = (int)(leftTimeInMilliseconds / 1000);
		String string = String.format("%02d:%02d", seconds /60,seconds % 60);
		((TextView)findViewById(R.id.timeCountDown)).setText(string);
	    }

	    @Override
	    public void onFinish() {
		String string = String.format("%02d:%02d", 0,0);
		((TextView)findViewById(R.id.timeCountDown)).setText(string);
	    }
	}.start();
    }

}
